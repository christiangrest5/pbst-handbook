# Tier 4 and Trainer commands

:::warning These commands are for trainings "most" of the time.
Alot of the commands seen on this page, will be used by assistants, Tier 4's and trainers.   
Every PB facility has a permission system, so using these commands as a -T4 won't be possible.  

When hosting a training, or being in one as an assistant. The training is given in **ENGLISH**. So make sure you are speaking this fluently, or at least understandable.
:::

:::danger Don't just copy paste
Please note that these commands are **EXAMPLES** you have to change then to your training/the thing you need it to do.  
Make sure you've tested your commands before you use then in an **ACTUAL** training...
:::

## Possible arguments
- ``me`` -> Yourself.
- ``player-name`` -> A player you select.
- ``all`` -> Everyone in the server (So also yourself and observers).
- ``%team-name`` -> Selecting a team (**Example**: ``;add %Winners Win 1``).
- ``nonadmins`` -> Every user who doesn't have admin permissions (Most likely eveyone under Tier 4. **NOTE**: This means observers also can get selected with this argument).   
- ``radius-<distance>`` -> Everyone in the distance you specify. (**Example**: ``;team radius-20 winners``)
- ``others`` -> This selects everyone, except yourself (So if you have assistants, or observers you also select those)

## What commands are generally used in trainings?
- ``;fly | ;god``  
This string of commands gives you fly, and gods you (You can't take any damage)
- ``;team <argument> <team>``  
Put a player in a team, depending on what teams you created with the commands below 
- ``;newteam Host/Assistants <color>``  
When you are hosting a training, this command creates the Host/Assistant team.
- ``;newteam Observers <color>``  
Create a team for observers (People that watch the training. **NOTE**: These never get points)
- ``;newteam Winners <color>``  
A team to define the winners in a game, and give them wins (So you don't give everyone a win) 
- ``;newteam Security <color>``  
This is the default team, where everyone should be in (Except the host, observers and assistants)
![img](https://i.imgur.com/vLwSyFK.png)

- ``;addstat Wins``  
Add a statistic so you can see how many wins everyone has, and to log them
- ``;m <message>``  
This is an announcement you can see on screen, with the default roblox line. ![img](https://i.imgur.com/PcjEr9v.png) 
- ``;n <message>``  
A small note everyone sees on their screen ![img](https://i.imgur.com/rZKvc03.png)
- ``;h <message>``
![img](https://i.imgur.com/YXRuuJB.png)
A message sent to the top of everyone's screen
- ``;add <argument> Wins 1``  
Gives someone or a team a Win (Players don't need a %, but teams do).
- ``;team radius-<distance> <Team>``  
Teams people around you on a team, this is very useful for giving wins
- ``;give <argument> <tool>``  
Gives people a tool that is located in the toolbox
- ``;sword <argument>``  
A short command for giving people swords
- ``;removetools <argument>``  
Removes the tools of the selected people
- ``;viewtools <argument>``  
Makes you able to see the tools that a person has on them
- ``!importalias <!alias> <Command>``  
Use this command to add an alias to your own collection (Example: **!addalias !shirtme !shirt [146487695](https://www.roblox.com/catalog/146487695/Security-Vest-Specops-PB)**)

## Suggestions for aliases

 1. Open Kronos by clicking on the bottom right corner of your screen (PBST Logo)   
 2. Open the tab "Aliases"  
 3. Press "Add" on the bottom of the window.  
 4. Fill in the alias you would like for your command, in the field **Alias:**  
 5. Enter one of the commands down here, or one of your own commands  
  
**``;assist``**: 
- ``;team <arg1> Host/Assistants | ;fly <arg1> | ;god <arg1> | ;sword <arg1>``
Gives a person Assistant commands and puts them on the Host/Assistants team.
Trainers can also use extra commands to improve the alias:
- ``;team <arg1> Host/Assistants | ;admin <arg1> | ;loadout <arg1> Special``
Gives the person temp moderator for the training and the Tier 4 (Special Defense) Loadout.

**``;observe``**:   
- ``;team <arg1> Obs | ;fly <arg1> | ;god <arg1>``   
Puts someone on the observer team and gives them observer commands  
![img](https://i.imgur.com/imuV43Y.png)  

**``;s2vote``**:
- ``;vote nonadmins,-%ob Sword_Fight,Guns,Juggernaut,Bombs 20 What activity shall we do?``
Useful for hosts to easily select an arena within Sector 2.
  
**``;noreturn``**:  
- ``;lobbydoors off | ;teleporters off``  
Locks down the lobby so no one can get out of it  

**``;bafk``**:
- ``;bring <arg1> | ;afk <arg1>``
Brings the person to you and AFKs them.

**``;flygod``**:
- ``;fly <arg1> | ;god <arg1>``
This allows the selected argument to fly and be godded.

**``;bjail``**:
- ``;bring <arg1> | ;jail <arg1>``
Bring the person and jails them in front of you.

**``;win20``**: 
- ``;team radius-20 %Winners``

**``;twin``**: 
- ``;add %Winners Wins 1``

**``;unteamwin``**: 
- ``;unteam %Winners``
This process is used to easily give out wins to a certain group of people.

**``;trainingsetup``**:
- ``;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins``
- ``;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on``
- ``;newteam Host/Assistants <color> | ;newteam Observers <color> | ;newteam Winners <color> | ;newteam Security <color> | ;addstat Wins | ;training on | ;addstat Strikes`` (This one can also be used with **``;strike``**)
Creates every team you will need as well as giving the ‘Wins’ stat. To make the alias more advanced, there are several posted on this line.


**Credits to *MarusiFyren* and *EquilibriumCurse* for helping me create this page, and marusi for some of the screenshots used on this page.**